package com.adeeb.popmeals.task.data

import com.google.gson.annotations.SerializedName

data class OrderApiModel(

    @SerializedName("order_id") var orderId: Int,
    @SerializedName("arrives_at_utc") var arrivesAtUtc: Long? = null,
    @SerializedName("paid_with") var paidWith: String = "",
    @SerializedName("total_paid") var totalPaid: Double = 0.0
)

data class OrdersResponse(
    @SerializedName("orders") var orders: List<OrderApiModel> = arrayListOf()
)
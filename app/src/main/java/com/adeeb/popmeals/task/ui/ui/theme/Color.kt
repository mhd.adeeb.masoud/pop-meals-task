package com.adeeb.popmeals.task.ui.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val LightGray = Color(0xFFF2F3F3)
val PurplePrimary = Color(0xFF7056B5)
val TextPrimary = Color (0xFF131415)
val TextSecondary = Color (0xFF616468)
val ColorBackground = Color(0xFFFFFFFF)
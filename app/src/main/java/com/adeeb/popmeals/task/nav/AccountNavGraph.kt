package com.adeeb.popmeals.task.nav

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import com.adeeb.popmeals.task.ui.mockscreens.MyAddressesScreen
import com.adeeb.popmeals.task.ui.mockscreens.MyProfileScreen
import com.adeeb.popmeals.task.ui.mockscreens.OrderDetailsScreen
import com.adeeb.popmeals.task.ui.myaccount.AccountScreen

fun NavGraphBuilder.setupAccountNavGraph(
    closeCallback: () -> Unit,
    navController: NavHostController
) {
    composable(ACCOUNT_SCREEN) {
        AccountScreen(
            closeCallback = closeCallback,
            navigateToOrderDetails = {
                Screen.OrderDetailsScreen(it).navigate(navController)
            }, navigateToMyProfile = { Screen.ProfileScreen.navigate(navController) },
            navigateToMyAddresses = { Screen.AddressesScreen.navigate(navController) }
        )
    }

    composable(PROFILE_SCREEN) {
        MyProfileScreen(
            closeCallback = closeCallback
        )
    }

    composable(ADDRESSES_SCREEN) {
        MyAddressesScreen(
            closeCallback = closeCallback
        )
    }

    composable(
        route = ORDER_DETAILS_SCREEN_FULL
    ) {
        OrderDetailsScreen(
            closeCallback = closeCallback,
            orderId = it.arguments?.getString(ORDER_DETAILS_SCREEN_PARAM_ORDER_ID)?.toInt()
        )
    }
}
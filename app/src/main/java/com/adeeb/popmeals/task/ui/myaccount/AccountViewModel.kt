package com.adeeb.popmeals.task.ui.myaccount

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adeeb.popmeals.task.data.UiOrder
import com.adeeb.popmeals.task.usecase.LoadOrdersUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class AccountViewModel @Inject constructor(private val useCase: LoadOrdersUseCase) : ViewModel() {

    private val _orderList = MutableStateFlow<List<UiOrder>>(listOf())
    val orderList = _orderList.asStateFlow()

    private val _errorSnackBarText = MutableStateFlow<String?>(null)
    val errorSnackBarText = _errorSnackBarText.asStateFlow()

    private val _loadingOrders = MutableStateFlow(false)
    val loadingOrders = _loadingOrders.asStateFlow()

    private val _goBack = MutableStateFlow(false)
    val goBack = _goBack.asStateFlow()

    private val _shouldNavigateToProfile = MutableStateFlow(false)
    val shouldNavigateToProfile = _shouldNavigateToProfile.asStateFlow()

    private val _shouldNavigateToAddresses = MutableStateFlow(false)
    val shouldNavigateToAddresses = _shouldNavigateToAddresses.asStateFlow()

    private val _goToDestination = MutableStateFlow<Int?>(null)
    val goToDestination = _goToDestination.asStateFlow()

    init {
        loadOrders()
    }

    private fun loadOrders() {
        viewModelScope.launch {
            _loadingOrders.value = true
            try {
                _orderList.value = useCase.loadOrders()
            } catch (exception: HttpException) {
                if (exception.code() == 503) {
                    _errorSnackBarText.value = "Server Error 503."
                } else {
                    _errorSnackBarText.value =
                        "Unknown Server Error (${exception.code()}: ${exception.message()}"
                }
            } catch (exception: IOException) {
                _errorSnackBarText.value = "Please check your internet connection"
            } catch (exception: Exception) {
                _errorSnackBarText.value = "Unknown Exception: ${exception.message}"
            } finally {
                _loadingOrders.value = false
            }
        }
    }

    fun onLogoutClicked() {
        _goBack.value = true
    }

    fun onMyAddressesClicked() {
        _shouldNavigateToAddresses.value = true
    }

    fun onMyProfileClicked() {
        _shouldNavigateToProfile.value = true
    }

    fun onOrderClicked(order: UiOrder) {
        _goToDestination.value = order.id
    }

    fun onBackClicked() {
        _goBack.value = true
    }

    fun onSnackBarShown() {
        _errorSnackBarText.value = null
    }

    fun refresh() {
        loadOrders()
    }

    fun onNavigatedToOrder() {
        _goToDestination.value = null
    }

    fun onNavigatedToAddresses() {
        _shouldNavigateToAddresses.value = false
    }

    fun onNavigatedToProfile() {
        _shouldNavigateToProfile.value = false
    }
}
package com.adeeb.popmeals.task.ui.shared

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.adeeb.popmeals.task.ui.ui.theme.PurplePrimary
import com.adeeb.popmeals.task.ui.ui.theme.Typography

@Composable
fun MockScreen(@StringRes title: Int, content: String= "", closeCallback: () -> Unit = {}) {
    Column(modifier = Modifier.fillMaxSize()) {
        ScreenHeader(titleRes = title, closeCallback = closeCallback)
        Box(
            modifier = Modifier
                .weight(1f, true)
                .fillMaxWidth(), contentAlignment = Alignment.Center
        ) {
            Text(
                text = content,
                modifier = Modifier.padding(16.dp),
                style = Typography.h4,
                color = PurplePrimary,
                textAlign = TextAlign.Center,
            )

        }
    }
}
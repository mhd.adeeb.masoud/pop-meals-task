package com.adeeb.popmeals.task.nav

import androidx.navigation.NavController

const val ACCOUNT_SCREEN = "Account"
const val PROFILE_SCREEN = "Profile"
const val ADDRESSES_SCREEN = "Addresses"
const val ORDER_DETAILS_SCREEN_PARAM_ORDER_ID = "orderId"
const val ORDER_DETAILS_SCREEN_FULL = "OrderDetails/{$ORDER_DETAILS_SCREEN_PARAM_ORDER_ID}"
const val ORDER_DETAILS_SCREEN_KEY = "OrderDetails"

sealed class Screen(
    screenName: String,
    private val navigateCallback: (NavController) -> Unit = { it.navigate(screenName) }
) {
    fun navigate(navController: NavController) {
        navigateCallback(navController)
    }

    object AccountScreen : Screen(ACCOUNT_SCREEN)
    object ProfileScreen : Screen(PROFILE_SCREEN)
    object AddressesScreen : Screen(ADDRESSES_SCREEN)
    class OrderDetailsScreen(private val orderId: Int) :
        Screen(ORDER_DETAILS_SCREEN_FULL, {
            it.navigate("$ORDER_DETAILS_SCREEN_KEY/$orderId")
        })
}
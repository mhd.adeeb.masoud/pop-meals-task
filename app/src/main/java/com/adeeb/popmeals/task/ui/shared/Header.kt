package com.adeeb.popmeals.task.ui.shared

import androidx.annotation.StringRes
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import com.adeeb.popmeals.task.R
import com.adeeb.popmeals.task.ui.ui.theme.Typography

@Composable
fun ScreenHeader(
    @StringRes titleRes: Int,
    closeCallback: () -> Unit = {}
) {
    Icon(
        ImageVector.vectorResource(id = R.drawable.back_arrow),
        "Back button",
        modifier = Modifier
            .padding(PaddingValues(top = 56.dp, start = 16.dp, end = 16.dp))
            .size(15.dp)
            .clickable(onClick = closeCallback)
    )

    Row(modifier = Modifier.padding(PaddingValues(start = 16.dp, end = 16.dp, top = 32.dp))) {
        Text(
            text = stringResource(titleRes),
            style = Typography.h1
        )
//        if (showLoading)
//            CircularProgressIndicator(
//                modifier = Modifier
//                    .align(Alignment.Bottom)
//                    .padding(start = 16.dp, bottom = 4.dp)
//                    .size(24.dp),
//                color = PurplePrimary
//            )
    }
}
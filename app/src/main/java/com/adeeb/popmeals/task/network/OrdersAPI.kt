package com.adeeb.popmeals.task.network

import androidx.annotation.WorkerThread
import com.adeeb.popmeals.task.data.OrdersResponse
import retrofit2.http.GET

interface OrdersAPI {

    @GET("/test/orders")
    @WorkerThread
    suspend fun getOrders() : OrdersResponse
}
package com.adeeb.popmeals.task.ui.myaccount

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.adeeb.popmeals.task.R
import com.adeeb.popmeals.task.ui.shared.ScreenHeader
import com.adeeb.popmeals.task.ui.ui.theme.*
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import kotlinx.coroutines.launch

@Composable
fun AccountScreen(
    modifier: Modifier = Modifier,
    viewModel: AccountViewModel = hiltViewModel(),
    closeCallback: () -> Unit = {},
    navigateToOrderDetails: (Int) -> Unit = {},
    navigateToMyProfile: () -> Unit = {},
    navigateToMyAddresses: () -> Unit = {}
) {
    val scaffoldState = rememberScaffoldState()
    Scaffold(modifier = Modifier.fillMaxSize(), scaffoldState = scaffoldState) {

        HandleAccountNavigation(
            viewModel,
            closeCallback,
            navigateToMyAddresses,
            navigateToMyProfile,
            navigateToOrderDetails
        )

        HandleAccountSnackBar(viewModel, scaffoldState)

        val orderList by viewModel.orderList.collectAsState()
        val orderListLoading by viewModel.loadingOrders.collectAsState()

        SwipeRefresh(
            state = rememberSwipeRefreshState(isRefreshing = orderListLoading),
            onRefresh = { viewModel.refresh() }) {
            Column(
                modifier = modifier
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState())
            ) {
                ScreenHeader(R.string.my_account, viewModel::onBackClicked)

                OrderList(
                    modifier = Modifier
                        .padding(PaddingValues(top = 15.dp))
                        .animateContentSize(),
                    orderList = orderList,
                    onOrderClicked = viewModel::onOrderClicked
                )

                AccountRow(
                    icon = R.drawable.profile,
                    text = R.string.my_profile,
                    onClick = viewModel::onMyProfileClicked
                )

                AccountRow(
                    icon = R.drawable.address,
                    text = R.string.my_addresses,
                    onClick = viewModel::onMyAddressesClicked
                )

                Spacer(
                    modifier = Modifier
                        .weight(1f, true)
                        .defaultMinSize(minHeight = 40.dp)
                )

                val interactionSource = remember { MutableInteractionSource() }
                Text(
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .padding(bottom = 40.dp)
                        .clickable(
                            interactionSource = interactionSource,
                            indication = null,
                            onClick = viewModel::onLogoutClicked
                        ),
                    text = stringResource(id = R.string.logout),
                    style = Typography.subtitle1
                )

            }
        }
    }
}

@Composable
private fun HandleAccountNavigation(
    viewModel: AccountViewModel,
    closeCallback: () -> Unit,
    navigateToMyAddresses: () -> Unit,
    navigateToMyProfile: () -> Unit,
    navigateToOrderDetails: (Int) -> Unit
) {
    val goBack by viewModel.goBack.collectAsState()
    val destinationOrderId by viewModel.goToDestination.collectAsState()
    val shouldNavigateToProfile by viewModel.shouldNavigateToProfile.collectAsState()
    val shouldNavigateToAddresses by viewModel.shouldNavigateToAddresses.collectAsState()

    if (goBack) {
        closeCallback()
    } else if (shouldNavigateToAddresses) {
        navigateToMyAddresses()
        viewModel.onNavigatedToAddresses()
    } else if (shouldNavigateToProfile) {
        navigateToMyProfile()
        viewModel.onNavigatedToProfile()
    } else if (destinationOrderId != null) {
        navigateToOrderDetails(destinationOrderId!!)
        viewModel.onNavigatedToOrder()
    }
}

@Composable
private fun HandleAccountSnackBar(
    viewModel: AccountViewModel,
    scaffoldState: ScaffoldState
) {
    val errorSnackBarText by viewModel.errorSnackBarText.collectAsState()
    val coroutineScope = rememberCoroutineScope()
    errorSnackBarText?.let {
        coroutineScope.launch {
            scaffoldState.snackbarHostState.showSnackbar(it)
        }
        viewModel.onSnackBarShown()
    }
}


@Composable
fun AccountRow(
    modifier: Modifier = Modifier,
    @DrawableRes icon: Int,
    @StringRes text: Int,
    onClick: () -> Unit = {}
) {
    val interactionSource = remember { MutableInteractionSource() }
    Row(
        modifier = modifier
            .padding(PaddingValues(top = 16.dp, start = 16.dp, end = 16.dp))
            .clickable(
                interactionSource = interactionSource,
                indication = null,
                onClick = onClick
            ),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            modifier = Modifier.size(24.dp),
            imageVector = ImageVector.vectorResource(id = icon),
            contentDescription = null,
            tint = PurplePrimary
        )
        Text(
            modifier = Modifier
                .weight(1f, true)
                .padding(horizontal = 8.dp),
            text = stringResource(id = text),
            style = Typography.subtitle1
        )
        Icon(
            modifier = Modifier.size(16.dp),
            imageVector = ImageVector.vectorResource(id = R.drawable.right_arrow),
            contentDescription = null
        )
    }
}


@Preview(widthDp = 420, heightDp = 720, showBackground = true)
@Composable
fun DefaultPreview() {
    PopMealsTaskTheme {
        AccountScreen()
    }
}
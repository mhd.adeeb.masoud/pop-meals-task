package com.adeeb.popmeals.task.usecase

import com.adeeb.popmeals.task.data.UiOrder
import com.adeeb.popmeals.task.network.OrdersAPI
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class LoadOrdersUseCase @Inject constructor(private val api: OrdersAPI) {

    private val formatter = SimpleDateFormat("HH:mma")

    private fun formatDate(arrivesAtUtc: Long?): String {
        return arrivesAtUtc?.let { formatter.format(Date(arrivesAtUtc)) } ?: "Unknown"
    }

    suspend fun loadOrders(): List<UiOrder> = api.getOrders().orders
        .filter { it.arrivesAtUtc != null }
        .sortedBy { it.arrivesAtUtc }
        .map {
            UiOrder(
                it.orderId,
                formatDate(it.arrivesAtUtc),
                "Delivered",
                it.paidWith
            )
        }

}
package com.adeeb.popmeals.task.ui.mockscreens

import androidx.compose.runtime.Composable
import com.adeeb.popmeals.task.R
import com.adeeb.popmeals.task.ui.shared.MockScreen

@Composable
fun OrderDetailsScreen(orderId: Int? = null, closeCallback: () -> Unit = {}) {
    MockScreen(title = R.string.order_details, "Order #$orderId", closeCallback = closeCallback)
}

@Composable
fun MyProfileScreen(closeCallback: () -> Unit = {}) {
    MockScreen(title = R.string.my_profile, "Mock Profile", closeCallback = closeCallback)
}

@Composable
fun MyAddressesScreen(closeCallback: () -> Unit = {}) {
    MockScreen(title = R.string.my_addresses, "Mock Addresses", closeCallback = closeCallback)
}
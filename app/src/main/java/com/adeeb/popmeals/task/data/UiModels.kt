package com.adeeb.popmeals.task.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UiOrder(
    val id: Int,
    val arrivedAt: String,
    val status: String,
    val paymentMethod: String
) : Parcelable

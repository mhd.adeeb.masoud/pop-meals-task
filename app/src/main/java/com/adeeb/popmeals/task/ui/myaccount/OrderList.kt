package com.adeeb.popmeals.task.ui.myaccount

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.adeeb.popmeals.task.R
import com.adeeb.popmeals.task.data.UiOrder
import com.adeeb.popmeals.task.ui.ui.theme.*

@Composable
fun OrderList(
    modifier: Modifier = Modifier,
    orderList: List<UiOrder>,
    onOrderClicked: (UiOrder) -> Unit = {}
) {
    val listState = rememberLazyListState()

    val visibleItem = remember {
        derivedStateOf {
            listState.layoutInfo.visibleItemsInfo.lastOrNull()?.index ?: 1
        }
    }

    val listSize = orderList.size

    Column(modifier = modifier) {
        if (listSize == 0) {
            Box(
                modifier = Modifier
                    .height(165.dp)
                    .fillMaxWidth()
                    .align(Alignment.CenterHorizontally),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    modifier = Modifier
                        .padding(16.dp),
                    text = stringResource(id = R.string.empty_orders),
                    style = Typography.subtitle1,
                    color = TextSecondary,
                    textAlign = TextAlign.Center,
                )
            }
        } else {
            OrdersList(listState, Modifier, orderList, onOrderClicked)
            OrdersListIndicator(
                Modifier.Companion
                    .align(Alignment.CenterHorizontally)
                    .padding(top = 10.dp),
                listSize,
                visibleItem
            )
        }
    }

}

@Composable
private fun OrdersList(
    listState: LazyListState,
    modifier: Modifier,
    orderList: List<UiOrder>,
    onOrderClicked: (UiOrder) -> Unit
) {
    LazyRow(
        state = listState,
        modifier = modifier,
        horizontalArrangement = Arrangement.spacedBy(11.dp),
        contentPadding = PaddingValues(horizontal = 16.dp),
        reverseLayout = true
    ) {
        items(orderList, { it.id }) { order ->
            OrderItem(
                orderNumber = "#${order.id}",
                arrivalTime = order.arrivedAt,
                paymentMethod = order.paymentMethod,
                status = order.status,
                onCardClicked = { onOrderClicked(order) }
            )
        }
    }
}

@Composable
private fun OrdersListIndicator(
    modifier: Modifier = Modifier,
    listSize: Int,
    visibleItem: State<Int>
) {

    if (listSize < 10) {
        DotsIndicator(
            modifier = modifier,
            totalDots = listSize - 1,
            selectedIndex = listSize - visibleItem.value - 1,
            selectedColor = TextPrimary,
            unSelectedColor = LightGray
        )
    } else {
        TextIndicator(
            modifier = modifier,
            index = visibleItem.value,
            total = listSize - 1
        )
    }
}

@Composable
fun OrderItem(
    modifier: Modifier = Modifier,
    orderNumber: String,
    arrivalTime: String,
    paymentMethod: String,
    status: String,
    onCardClicked: () -> Unit = {}
) {
    Column(
        modifier = modifier
            .size(326.dp, 165.dp)
            .background(LightGray, RoundedCornerShape(4.dp))
            .clickable(onClick = onCardClicked)
    ) {

        Text(
            modifier = Modifier.padding(PaddingValues(top = 16.dp, start = 16.dp)),
            text = stringResource(id = R.string.order_number),
            style = Typography.h4
        )

        Text(
            modifier = Modifier.padding(PaddingValues(top = 4.dp, start = 16.dp)),
            text = orderNumber,
            style = Typography.caption,
            color = PurplePrimary
        )
        Row {
            Column(
                modifier = Modifier
                    .padding(
                        PaddingValues(
                            top = 16.dp,
                            start = 16.dp
                        )
                    )
                    .width(116.dp),
            ) {
                arrayOf(R.string.arrives_at, R.string.paid_with, R.string.status).forEach {
                    Text(
                        style = Typography.caption,
                        text = stringResource(id = it),
                    )
                }
            }

            Column(
                modifier = Modifier
                    .padding(
                        PaddingValues(
                            top = 16.dp,
                        )
                    )
                    .width(64.dp)
            ) {
                arrayOf(arrivalTime, paymentMethod, status).forEach {
                    Text(
                        style = Typography.caption,
                        text = it,
                        color = TextSecondary
                    )
                }
            }

        }
    }
}


@Preview(widthDp = 340, heightDp = 200, showBackground = true)
@Composable
fun OrderListPreview() {
    PopMealsTaskTheme {
        OrderList(orderList = mockList())
    }
}

@Preview(widthDp = 500, heightDp = 200, showBackground = true)
@Composable
fun OrderItemPreview() {
    PopMealsTaskTheme {
        OrderItem(
            orderNumber = "#87487324",
            arrivalTime = "06:00pm",
            paymentMethod = "Cash",
            status = "Delivered"
        )
    }
}


private fun mockList() = (1..9).map {
    UiOrder(
        it,
        "0$it:00",
        "Delivered",
        "Cash"
    )
}

package com.adeeb.popmeals.task.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.adeeb.popmeals.task.nav.*
import com.adeeb.popmeals.task.ui.ui.theme.ColorBackground
import com.adeeb.popmeals.task.ui.ui.theme.PopMealsTaskTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PopMealsTaskTheme {
                val navController = rememberNavController()
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = ColorBackground
                ) {
                    NavHost(navController = navController, startDestination = ACCOUNT_SCREEN) {
                        val closeCallback = {
                            if (!navController.popBackStack()) finish()
                        }
                        setupAccountNavGraph(closeCallback, navController)
                    }
                }
            }
        }
    }
}


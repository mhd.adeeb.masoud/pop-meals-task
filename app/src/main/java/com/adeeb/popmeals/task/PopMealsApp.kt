package com.adeeb.popmeals.task

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PopMealsApp : Application()